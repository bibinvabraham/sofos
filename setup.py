from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in sofos/__init__.py
from sofos import __version__ as version

setup(
	name="sofos",
	version=version,
	description="SoPos",
	author="Bibin V Abraham",
	author_email="bibinvabraham1@gmail.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
